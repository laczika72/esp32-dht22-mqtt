// DHT Temperature & Humidity Sensor
// Unified Sensor Library Example
// Written by Tony DiCola for Adafruit Industries
// Released under an MIT license.

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ESPmDNS.h>
#include <NTPClient.h>

// LED and PWM properties
#define RED_LED_PIN 15
#define GREEN_LED_PIN 0
#define BLUE_LED_PIN 4

#define RED_LED_CHANNEL 0
#define GREEN_LED_CHANNEL 1
#define BLUE_LED_CHANNEL 2

#define PWM_FREQUENCY 5000
#define PWM_RESOLUTION 8

#define DHTPIN 13 // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment the type of sensor in use:
//#define DHTTYPE    DHT11     // DHT 11
#define DHTTYPE DHT22 // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

// See guide for details on sensor wiring and usage:
//   https://learn.adafruit.com/dht/overview

DHT_Unified dht(DHTPIN, DHTTYPE);
uint32_t delayMS;

const char *ssid = "<<my ssid>>";
const char *password = "<<my password>>";

WiFiClient espClient;
PubSubClient pubSubClient(espClient);

/* topics */
#define HUMIDITY_TOPIC "mypi/groundfloor/bathroom/sensor/humidity"
#define TEMPERATURE_TOPIC "mypi/groundfloor/bathroom/sensor/temperature"

const char *serverHostname = "happy";

#define NTP_OFFSET 60 * 60     // In seconds
#define NTP_INTERVAL 60 * 1000 // In miliseconds
#define NTP_ADDRESS "europe.pool.ntp.org"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

void mqttConnect()
{
  /* Loop until reconnected */
  while (!pubSubClient.connected())
  {
    Serial.print("MQTT connecting ...");
    /* client ID */
    String clientId = "MyPi-Groundfloor-Bathroom";

    /* connect now */
    if (pubSubClient.connect(clientId.c_str()))
    {
      Serial.println("connected");
      /* subscribe topic */
      // pubSubClient.subscribe(LED_TOPIC);
    }
    else
    {
      Serial.print("failed, status code =");
      Serial.println(pubSubClient.state());
      Serial.println("try again in 5 seconds");
      /* Wait 5 seconds before retrying */
      delay(5000);
    }
  }
}

void wifiReconnect()
{
  if (WiFi.status() != WL_CONNECTED)
  {
    Serial.println();
    Serial.println();
    Serial.print("WiFi connection lost, reconnecting to ");
    Serial.println(ssid);
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }
  }
}

void fadeLed(uint8_t channel)
{
  // increase the LED brightness
  for (int dutyCycle = 0; dutyCycle <= 100; dutyCycle++)
  {
    // changing the LED brightness with PWM
    ledcWrite(channel, dutyCycle);
    delay(15);
  }

  // decrease the LED brightness
  for (int dutyCycle = 100; dutyCycle >= 0; dutyCycle--)
  {
    // changing the LED brightness with PWM
    ledcWrite(channel, dutyCycle);
    delay(15);
  }
}

void ledColor(uint32_t red, uint32_t green, uint32_t blue)
{
  ledcWrite(RED_LED_CHANNEL, red);
  ledcWrite(GREEN_LED_CHANNEL, green);
  ledcWrite(BLUE_LED_CHANNEL, blue);
}

void ledYellow()
{
  ledColor(100, 255, 0); // Yellow
}

void ledRed()
{
  ledColor(255, 0, 0); // Red
}

void ledGreen()
{
  ledColor(0, 255, 0); // Green
}

void setup()
{
  Serial.begin(115200);

  Serial.println(F("------------------------------------"));
  Serial.println("Setting up RGB LED pins");

  ledcSetup(RED_LED_CHANNEL, PWM_FREQUENCY, PWM_RESOLUTION);
  ledcAttachPin(RED_LED_PIN, RED_LED_CHANNEL);

  ledcSetup(GREEN_LED_CHANNEL, PWM_FREQUENCY, PWM_RESOLUTION);
  ledcAttachPin(GREEN_LED_PIN, GREEN_LED_CHANNEL);

  ledcSetup(BLUE_LED_CHANNEL, PWM_FREQUENCY, PWM_RESOLUTION);
  ledcAttachPin(BLUE_LED_PIN, BLUE_LED_CHANNEL);

  // fadeLed(RED_LED_CHANNEL);
  // fadeLed(GREEN_LED_CHANNEL);
  // fadeLed(BLUE_LED_CHANNEL);

  // ledColor(255, 0, 0); // Red
  // delay(2000);
  // ledColor(0, 255, 0); // Green
  // delay(2000);
  // ledColor(0, 0, 255); // Blue
  // delay(2000);
  // ledColor(100, 255, 0); // Yellow
  // delay(2000);
  // ledColor(255, 0, 255); // Magenta
  // delay(2000);
  // ledColor(0, 255, 255); // Cyan

  ledYellow();

  Serial.println(F("------------------------------------"));
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  timeClient.begin();

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address :\t" + WiFi.localIP().toString());
  Serial.println("MAC address:\t" + WiFi.macAddress());
  Serial.println(F("------------------------------------"));

  /*setup MDNS for ESP32 */
  if (!MDNS.begin("esp32"))
  {
    Serial.println("Error setting up MDNS responder!");
    while (1)
    {
      delay(1000);
    }
  }
  /* get the IP address of server by MDNS name */
  Serial.println("mDNS responder started");
  IPAddress serverIp = MDNS.queryHost(serverHostname);
  Serial.print("IP address of server: ");
  Serial.println(serverIp.toString());
  pubSubClient.setServer(serverIp, 8883);
  Serial.println(F("------------------------------------"));

  // Initialize device.
  dht.begin();
  //dht2.begin();
  Serial.println(F("DHTxx Unified Sensor Example"));
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print(F("Sensor Type: "));
  Serial.println(sensor.name);
  Serial.print(F("Driver Ver:  "));
  Serial.println(sensor.version);
  Serial.print(F("Unique ID:   "));
  Serial.println(sensor.sensor_id);
  Serial.print(F("Max Value:   "));
  Serial.print(sensor.max_value);
  Serial.println(F("°C"));
  Serial.print(F("Min Value:   "));
  Serial.print(sensor.min_value);
  Serial.println(F("°C"));
  Serial.print(F("Resolution:  "));
  Serial.print(sensor.resolution);
  Serial.println(F("°C"));
  Serial.println(F("------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print(F("Sensor Type: "));
  Serial.println(sensor.name);
  Serial.print(F("Driver Ver:  "));
  Serial.println(sensor.version);
  Serial.print(F("Unique ID:   "));
  Serial.println(sensor.sensor_id);
  Serial.print(F("Max Value:   "));
  Serial.print(sensor.max_value);
  Serial.println(F("%"));
  Serial.print(F("Min Value:   "));
  Serial.print(sensor.min_value);
  Serial.println(F("%"));
  Serial.print(F("Resolution:  "));
  Serial.print(sensor.resolution);
  Serial.println(F("%"));
  Serial.println(F("------------------------------------"));
  // Set delay between sensor readings based on sensor details.
  Serial.print(F("Min delay :  "));
  Serial.println(sensor.min_delay);
  delayMS = sensor.min_delay / 1000 + 3000;

  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
  ledGreen();
}

void loop()
{
  // Delay between measurements.
  delay(delayMS);
  ledYellow();
  char mqttMsg[255];

  // Reconnect WiFi and MQTT when lost connection
  wifiReconnect();
  if (!espClient.connected())
    mqttConnect();

  // Query current time
  timeClient.update();
  String formattedTime = timeClient.getFormattedTime();
  char cTimestamp[128];
  formattedTime.toCharArray(cTimestamp, 128);

  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature))
  {
    Serial.println(F("Error reading temperature!"));
  }
  else
  {
    float temperature = event.temperature;
    Serial.print(F("Temperature: "));
    Serial.print(temperature);
    Serial.println(F("°C"));
    snprintf(mqttMsg, 255, "{\"timestamp\": \"%s\", \"temperature\": %f}", cTimestamp, temperature);
    pubSubClient.publish(TEMPERATURE_TOPIC, mqttMsg);
  }

  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity))
  {
    Serial.println(F("Error reading humidity!"));
  }
  else
  {
    float humidity = event.relative_humidity;
    Serial.print(F("Humidity: "));
    Serial.print(humidity);
    Serial.println(F("%"));
    snprintf(mqttMsg, 255, "{\"timestamp\": \"%s\", \"humidity\": %f}", cTimestamp, humidity);
    pubSubClient.publish(HUMIDITY_TOPIC, mqttMsg);
  }
  ledGreen();
  // Serial.println("------------------------------------------------");
  // float h = dht2.readHumidity();
  // // Read temperature as Celsius (the default)
  // float t = dht2.readTemperature();
  // // Read temperature as Fahrenheit (isFahrenheit = true)
  // float f = dht2.readTemperature(true);
  // // Check if any reads failed and exit early (to try again).
  // if (isnan(h) || isnan(t) || isnan(f))
  // {
  //   Serial.println("Failed to read from DHT sensor!");
  //   strcpy(celsiusTemp, "Failed");
  //   strcpy(fahrenheitTemp, "Failed");
  //   strcpy(humidityTemp, "Failed");
  // }
  // else
  // {
  //   // Computes temperature values in Celsius + Fahrenheit and Humidity
  //   float hic = dht2.computeHeatIndex(t, h, false);
  //   dtostrf(hic, 6, 2, celsiusTemp);
  //   float hif = dht2.computeHeatIndex(f, h);
  //   dtostrf(hif, 6, 2, fahrenheitTemp);
  //   dtostrf(h, 6, 2, humidityTemp);
  //   // You can delete the following Serial.print's, it's just for debugging purposes
  //   Serial.print("Humidity: ");
  //   Serial.print(h);
  //   Serial.print(" %\t Temperature: ");
  //   Serial.print(t);
  //   Serial.print(" *C ");
  //   Serial.print(f);
  //   Serial.print(" *F\t Heat index: ");
  //   Serial.print(hic);
  //   Serial.print(" *C ");
  //   Serial.print(hif);
  //   // Serial.print(" *F");
  //   // Serial.print("Humidity: ");
  //   // Serial.print(h);
  //   // Serial.print(" %\t Temperature: ");
  //   // Serial.print(t);
  //   // Serial.print(" *C ");
  //   // Serial.print(f);
  //   // Serial.print(" *F\t Heat index: ");
  //   // Serial.print(hic);
  //   // Serial.print(" *C ");
  //   // Serial.print(hif);
  //   Serial.println(" *F");
  // }
}
