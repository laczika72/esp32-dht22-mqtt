# ESP32-DHT22-MQTT

Sample ESP32 application polling relative humidity and temperature values from DHT22 sensor and pushing these data to an MQTT message broker.